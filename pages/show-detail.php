<!DOCTYPE html>
<html>

<head>
    <?php 
    include "../includes/connect.php";
    include_once '../includes/functions.php';
    sec_session_start();
    ?>
    <link rel="stylesheet" href="../css/main.css" type="text/css">
    <title>Appointment</title>
    <style>
        .field{
            margin: 15px;
            padding: 10px;
            word-wrap: break-word;
        }
    </style>
</head>

<body>
    <header>
        <div class="nav">
            <ul>
                <li><a href="appointment.php">Add appointment</a></li>
                <li><a href="month.php">Calendar</a></li>
                <li style="float:right";><a href="../includes/logout.php">Logout</a></li>
                <li style="float:right";><a href="#"><?php echo htmlentities($_SESSION['username']);?></a></li>
            </ul>
        </div>
    </header>
    <div class="form">
        <?php
            $num_id = $_GET['id'];
            $query = mysqli_query($link,"SELECT * FROM event WHERE id = '$num_id'");
            while($num_rows = mysqli_fetch_array($query)){
            echo '<h2>The appointment</h2>';
            echo '<div class="field">';
            echo 'Date : '.$num_rows[1];
            echo '</div>';
            echo '<div class="field">';
            echo 'Time : '.$num_rows[2];
            echo '</div>';
            echo '<div class="field">';
            echo 'Title : '.$num_rows[3];
            echo '</div>';
            echo '<div class="field">';
            echo 'Detail : '.$num_rows[4];
            echo '</div>';
            echo '<a href="edit-detail.php?id='.$num_id.'&date='.$num_rows[1].'&title='.$num_rows[2].'&detail='.$num_rows[3].'">';
            echo '<input class="bt-edit" type="button" value="Edit" />';
            echo '</a>'; 
            echo '<a href="../module/delete.php?id='.$num_id.'">';
            echo '<input class="bt-delete" type="button" value="Delete" />';
            echo '</a>';    
            echo '<a href="month.php">';
            echo '<input class="bt-back" type="button" value="Back to calendar"/>';
            echo '<br>';
        }
        ?>
    </div> 
</body>