<?php
/**
 * Copyright (C) 2013 peredur.net
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
include_once '../includes/db_connect.php';
include_once '../includes/functions.php';

sec_session_start();
?>
<!DOCTYPE html>
<html>
    <head>
            <link rel="stylesheet" href="../css/main.css" type="text/css">
            <script src="jscript.js"></script>
            <title>Create new event form</title>
    </head>
    <body>
        <?php if (login_check($mysqli) == true) : ?>
            <header>
        <div class="nav">
            <ul>
                <li><a class="active" href="main.php">Add appointment</a></li>
                <li><a href="month.php">Calendar</a></li>
                <li style="float:right";><a href="../includes/logout.php">Logout</a></li>
                <li style="float:right";><a href="#"><?php echo htmlentities($_SESSION['username']);?></a></li>
        </div>
    </header>
    <div class="form">
        <form id="form_regis" action="../module/insert.php" method="post" onsubmit="return validation()">
            <h2>Add appointment</h2>
            <div class="field"> Date :
                <input type="date" id="date" name="date" value="<?php echo date('Y-m-d'); ?>"> </div>
            <div class="field"> Time :
                <input type="time" id="time" name="time"> </div>
            <div class="field"> Title :
                <input type="text" id="title" name="title" placeholder="Title" oninput="check()"> </div>
            <div class="field"> 
                <textarea name="detail" id="detail"></textarea>
            </div>
            <button type="submit">Create</button>
        </form>
    </div>
        <?php else : ?>
            <p>
                <span class="error">You are not authorized to access this page.</span> Please <a href="../index.php">login</a>.
            </p>
        <?php endif; ?>
    </body>
</html>
