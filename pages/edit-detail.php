<!DOCTYPE html>
<html>

<head>
    <?php include_once '../includes/functions.php';
    sec_session_start();
    ?>
    <link rel="stylesheet" href="../css/main.css" type="text/css">
    <script src="jscript.js"></script>
    <title>Edit appointment</title>
</head>

<body>
    <header>
        <div class="nav">
            <ul>
                <li><a href="appointment.php">Add appointment</a></li>
                <li><a href="month.php">Calendar</a></li>
                <li style="float:right";><a href="../includes/logout.php">Logout</a></li>
                <li style="float:right";><a href="#"><?php echo htmlentities($_SESSION['username']);?></a></li>
            </ul>
        </div>
    </header>
    <div class="form">
        <?php
        $num_id = $_GET['id'];
        $date = $_GET['date'];
        $title = $_GET['title'];
        $detail = $_GET['detail'];
        ?>
        <form id="form_regis" action="../module/edit.php" method="post" onsubmit="return validation()">
            <input type="hidden" name="id" value="<?php echo $num_id;?>"/>
            <h2>Edit appoitment</h2>
            <div class="field"> Date :
                <input type="date" id="date" name="date" value="<?php echo $date;?>"/></div>
            <div class="field"> Title :
                <input type="text" id="title" name="title" value="<?php echo $title;?>"/></div>
            <div class="field"> 
                <textarea name="detail" id="detail"/><?php echo $detail;?></textarea>
            </div>
            <button type="submit">Edit</button>
        </form>
    </div>
</body>