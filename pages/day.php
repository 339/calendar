<?php
    include_once '../includes/functions.php';
    sec_session_start();
    $session_username = htmlentities($_SESSION['username']);
    date_default_timezone_set("Asia/Bangkok"); //set time zone
    $now = 'now'; // declare now for use in find year-month-day
    if(isset($_GET['now']) && !empty($_GET['now'])){
    $now = $_GET['now'];
    }
    $day = date('d',strtotime($now)); 
    $month = date('m',strtotime($now)); // get month for use in fucntion next and prev.
    $year = date('Y', strtotime($now)); // get year for use in fucntion next and prev.
    $firstday = date('w', strtotime('01-'.$month.'-'.$year)); // get day of week
    $days = date('t',strtotime($now)); // get number of days in month
    $today = date('d'); // get today
    $todaymonth = date('m'); // get month
    $todayyear = date('Y'); // get year
    
    $next_month = strtotime("+1 day $year-$month-$day"); // calculate timestamp next month
    $last_month = strtotime("-1 day $year-$month-$day"); // calculate timestamp last month 
    
    $get_next_year = date('Y', $next_month); // get year from $last_month
    $get_last_year = date('Y', $last_month); // get year from $next_month

    $get_next_month = date('m', $next_month); // get month from $last_month
    $get_last_month = date('m', $last_month); // get month from $next_month

    $get_next_day = date('d', $next_month); // get month from $last_month
    $get_last_day = date('d', $last_month); // get month from $next_month
    
    $to_lastmonth = "<a href=\"" . '?now=' . $get_last_year .'-'. $get_last_month . '-'. $get_last_day ."\"> ❮ </a>"; // link to lastmonth
    $to_nextmonth = "<a href=\"" . '?now=' . $get_next_year .'-'. $get_next_month . '-'. $get_next_day . "\"> ❯ </a>"; // link to nextmonth

    $check_month = $month; // check month to string
    switch ($check_month) {
        case "01":
        $check_month = "January";
        break;
        case "02":
         $check_month = "February";
        break;
        case "03":
         $check_month = "March";
        break;
        case "04":
         $check_month = "April";
        break;
        case "05":
         $check_month = "May";
        break;
        case "06":
         $check_month = "June";
        break;
        case "07":
         $check_month = "July";
        break;
        case "08":
         $check_month = "August";
        break;
        case "09":
         $check_month = "September";
        break;
        case "10":
         $check_month = "October";
        break;
        case "11":
         $check_month = "November";
        break;
        case "12":
         $check_month = "December";
        break;
}  
?>


<html>
<head>
    <title>Calendar</title>
    <link rel="stylesheet" href="../css/day.css" type="text/css">
</head>

<body>
    <header>
        <div class="nav">
            <ul>
                <li><a href="appointment.php">Add appointment</a></li>
                <li><a class="active" href="day.php">Day</a></li>
                <li><a href="week.php">Week</a></li>
                <li><a href="month.php">Month</a></li>
                <li style="float:right";><a href="../includes/logout.php">Logout</a></li>
                <li style="float:right";><a href="#"><?php echo htmlentities($_SESSION['username']);?></a></li>
            </ul>
        </div>
    </header>
    <div class="calendar">
        
        <div class="head-date">
        <?php echo $to_lastmonth . $day."/".$check_month."/".$year . $to_nextmonth;?>
        </div>
       
        
       
        <?php

    include "../includes/connect.php"; // connect to db
    for($i=0;$i<=23;$i++)
    {
        echo'<div class="date';
        echo '">'.$i.':00'.'<br>';    
        $date = $year.'/'.$month.'/'.$day;
        $query = mysqli_query($link,"SELECT * FROM event WHERE date = '$date' AND EXTRACT(HOUR FROM time) = '$i' AND users = '$session_username' ORDER BY time"); // query and select all data in table.
        while($num_rows = mysqli_fetch_array($query)){ // fetch array for read data in table.
            echo '<a class="title" href="show-detail.php?id='.$num_rows[0].'">';
            echo $num_rows[3];
            
            echo '</a>';
            echo '&nbsp';
            /*
            echo'<form action="show-detail.php" method="post';
            echo '">';
            echo'<input name="date" type="hidden" value="'.$date.'">'; // send date to show-detail.php
            echo'<input name="title" type="hidden" value="'.$num_rows[2].'">'; // send title to show-detail.php
            echo'<input name="detail" type="hidden" value="'.$num_rows[3].'">'; // send detail to show-detail.php
            echo '<button type="submit">';
            echo $num_rows[2]; // show title from DB
            echo '</button>';
            echo '</form>';
            */
            }
        echo '</div>';
            
    }

?>    
    </div>
</body>
</html>


